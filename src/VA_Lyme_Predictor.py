import numpy
import theano
import matplotlib.pyplot as plt
from pandas import read_csv
import math
from keras.models import Sequential
from keras.layers import Dense, Activation
from keras.layers import LSTM, SimpleRNN, GRU
from keras.layers.core import Dropout
from keras.layers.advanced_activations import LeakyReLU, PReLU
from sklearn.preprocessing import MinMaxScaler
from sklearn.metrics import mean_squared_error
from sklearn.decomposition import PCA
import sys
import os

#convert an array of values into a dataset matrix
def create_dataset(dataset, look_back=3, use_pca=False, eigenvector=None):
	dataX = None
	dataY = []
	monthlyDataHistory = None
	for i in range(len(dataset)-look_back-1):
		j = i
		#Append previous months
		#For example, if we're using months k-2, k-1, and k,
		#	here we'll append months k-2 and k-1
		while(j < i +look_back):
			monthlyData = numpy.array(dataset[j, 0:15])
			print("Monthly data shape: " + str(monthlyData.shape))
			monthlyData = monthlyData.reshape(1,1,monthlyData.shape[0])
			if monthlyDataHistory is None:
				monthlyDataHistory = monthlyData
			else:
				monthlyDataHistory = numpy.concatenate((monthlyDataHistory,monthlyData), axis=1)
			j += 1
		#Now append month k
		monthlyData = numpy.array(dataset[i+look_back, 0:14])
		#Zero pad the first value
		monthlyData = numpy.insert(monthlyData,0,0, axis=1)
		if dataX is None:
			dataX = monthlyDataHistory
		else:
			dataX = numpy.concatenate((dataX, monthlyDataHistory),axis=0)
		dataY.append(dataset[i + look_back, 14])
	return dataX, numpy.array(dataY)

#Calculates number of nodes in the hidden layer http://hagan.okstate.edu/NNDesign.pdf#page=469
#Where alpha is an arbitrary scaling factor usually 2-10
def calc_num_hidden_nodes(alpha, numInputsNodes, numOutputNodes, numSamples):
	return numSamples // ((numInputsNodes + numOutputNodes) * alpha)

def read_dataset(filename):
	# load the dataset
	dataframe = read_csv(filename, usecols=[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15], engine='python')
	dataset = dataframe.values
	dataset = dataset.astype('float32')
	return dataset

def create_brain(numRecords, numInputs, numOutputs, alpha, num_training_samples):
	numHiddenNodes = calc_num_hidden_nodes(alpha, numInputs, numOutputs, num_training_samples * numRecords)
	brain = Sequential()
	brain.add(LSTM(numInputs, input_shape=(1,numInputs), return_sequences=True, recurrent_activation='tanh'))
	brain.add(LSTM(numRecords, return_sequences=False, recurrent_activation='tanh'))
	brain.add(Dense(numOutputs))
	brain.compile(loss='mean_absolute_error', optimizer='adam')
	return brain

def train(brain, filename, look_back, alpha, num_training_samples, num_epochs, eigenvector):
	# load the dataset
	#For loop to read in files
	dataset = read_dataset(filename)
	# reshape into X=t and Y=t+3
	x, y = create_dataset(dataset, look_back, use_pca=True, eigenvector=eigenvector)
	# reshape input to be [samples, time steps, features]
	trainX = numpy.reshape(x, (x.shape[0], 1, x.shape[1]))
	if brain is None:
		brain = create_brain(x.shape[0], x.shape[1] ,1, alpha, num_training_samples)

	# create and fit the LSTM network
	brain.fit(trainX, y, epochs=num_epochs, batch_size=44, verbose=0)
	brain.reset_states()
	return brain

def test(brain, filename, look_back, eigenvector):
	# load the dataset
	#For loop to read in files
	dataset = read_dataset(filename)
	# reshape into X=t and Y=t+3
	x, y = create_dataset(dataset, look_back, use_pca=True, eigenvector=eigenvector)
	# reshape input to be [samples, time steps, features]
	testX = numpy.reshape(x, (x.shape[0], 1, x.shape[1]))
	#For each test file
	testPredict = brain.predict(testX, batch_size=1)
	#In order to report prediction error in original units, we mulitiply each
	#	prediction by 63 in order to "de-normalize"
	testPredictDenormed = testPredict * 63.0
	yDenormed = y * 63.0
	#Use the root mean square error to report approximate number of lyme cases mispredicted
	#	per four-year reporting period
	mseTestScore = mean_squared_error(yDenormed, testPredictDenormed[:,0])
	rmseTestScore = math.sqrt(mseTestScore)
	print('Test Score: %.2f RMSE, %.2f MSE' % (rmseTestScore, mseTestScore))
	#Predict
	score = brain.evaluate(testX, y,  verbose=0)
	rge = range(look_back, len(dataset)- 1 )
	plt.plot(dataset[:,14])
	plt.plot(rge,testPredict)
	return plt, rmseTestScore, mseTestScore

def determine_num_samples(training_directory):
	return len([name for name in os.listdir(training_directory) if os.path.isfile(os.path.join(training_directory, name))])

def build_eigenvector_from_training_data(training_directory, look_back):
	training_data = numpy.zeros(shape=(308,59))
	for filename in os.listdir(training_directory):
		trainingData = os.path.join(training_directory, filename)
		#For loop to read in files
		dataset = read_dataset(trainingData)
		# reshape into X=t and Y=t+3
		x, y = create_dataset(dataset, look_back)
		numpy.append(training_data, x)
	eigenvector = PCA(n_components=6)
	eigenvector.fit(training_data)
	return eigenvector



def accrue_knowledge(look_back, training_directory, testing_directory, alpha, num_training_samples, num_epochs):
	brain = None
	#Train network
	eigenvector = build_eigenvector_from_training_data(training_directory ,look_back)
	for filename in os.listdir(training_directory):
		brain = train(brain, os.path.join(training_directory, filename), look_back, alpha, num_training_samples, num_epochs, eigenvector)
	#Test network
	rmseScores = []
	for filename in os.listdir(testing_directory):
		#test data 
		plt, rmseTestScore, mseTestScore = test(brain, os.path.join(testing_directory, filename), look_back, eigenvector)
		rmseScores.append(rmseTestScore)
		#plot visualization and save to disk
		plt.savefig(filename + '_testing_performance.jpg')
		#clear plot for next iteration
		plt.clf()
	rmseAverage = 0
	rmseCounter = 0
	for rmseScore in rmseScores:
		rmseAverage += rmseScore
		rmseCounter+= 1
	print('Average Score: %.5f RMSE' % (rmseAverage / rmseCounter))


def main():
	training_directory = '/home/ktyler/Documents/strider/lyme/python/data/lagged_train_pruned_kfold'
	testing_directory = '/home/ktyler/Documents/strider/lyme/python/data/lagged_test_pruned_kfold'
	look_back = 3     #Number of months to use for prediction, i.e.: Use months x-3, x-2, and x-1 of data to predict month x
	alpha = 1 	  #Used to calculate number of hidden nodes
	num_epochs = 220  #Number of training iterations
	num_training_samples = determine_num_samples(training_directory)
	accrue_knowledge(look_back, training_directory, testing_directory, alpha, num_training_samples, num_epochs)

numpy.random.seed(7)
main()
