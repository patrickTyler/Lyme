import numpy
import theano
import matplotlib.pyplot as plt
from pandas import read_csv
import math
from keras.models import Sequential
from keras.layers import Dense, Activation
from keras.layers import LSTM, SimpleRNN, GRU
from sklearn.metrics import mean_squared_error
from sklearn import linear_model, svm, neighbors
from sklearn.decomposition import PCA
import sys
import os

# convert an array of values into a dataset matrix
def create_dataset(dataset, look_back=1):
    dataX, dataY = [], []
    for i in range(len(dataset) - look_back - 1):
        a = dataset[i:(i + look_back), 0:15]
        b = dataset[(i + look_back), 0:14]
        a = a.flatten()
        a = numpy.append(a, b)
        dataX.append(a)
        dataY.append(dataset[i + look_back, 14])
    data_matrix = numpy.array(dataX)
    return data_matrix, numpy.array(dataY)

# Calculates number of nodes in the hidden layer http://hagan.okstate.edu/NNDesign.pdf#page=469
# Where alpha is an arbitrary scaling factor usually 2-10
def calc_num_hidden_nodes(alpha, numInputsNodes, numOutputNodes, numSamples):
    return numSamples // ((numInputsNodes + numOutputNodes) * alpha)

def read_dataset(filename):
    # load the dataset
    dataframe = read_csv(filename, usecols=[1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15], engine='python')
    dataset = dataframe.values
    dataset = dataset.astype('float32')
    return dataset

def create_brain(numRecords, numInputs, numOutputs, alpha, num_training_samples):
    numHiddenNodes = calc_num_hidden_nodes(alpha, numInputs, numOutputs, num_training_samples * numRecords)
    brain = Sequential()
    brain.add(LSTM(numInputs, input_shape=(1, numInputs), return_sequences=True, recurrent_activation='tanh'))
    # brain.add(LSTM(numHiddenNodes, return_sequences=True, recurrent_activation='tanh'))
    brain.add(LSTM(numRecords, return_sequences=False, recurrent_activation='tanh'))
    brain.add(Dense(numOutputs))
    # brain.add(Dropout(.26))
    brain.compile(loss='mean_absolute_error', optimizer='adam')
    return brain

def train(brain, filename, look_back, alpha, num_training_samples, num_epochs):
    # load the dataset
    # For loop to read in files
    dataset = read_dataset(filename)
    # reshape into X=t and Y=t+3
    x, y = create_dataset(dataset, look_back)
    # reshape input to be [samples, time steps, features]
    trainX = numpy.reshape(x, (x.shape[0], 1, x.shape[1]))
    if brain is None:
        brain = create_brain(x.shape[0], x.shape[1], 1, alpha, num_training_samples)

    # create and fit the LSTM network
    brain.fit(trainX, y, epochs=num_epochs, batch_size=44, verbose=0)
    brain.reset_states()
    return brain

def deriveHealthDistrictName(filename):
    #Strip off path and extension
    formattedFileName = os.path.basename(os.path.splitext(filename)[0])
    if "MOUNTROGERS" in formattedFileName:
        formattedFileName = "Mount Rogers"
    elif "THOMASJEFFERSON" in formattedFileName:
        formattedFileName = "Thomas Jefferson"
    else:
        #Fix capitalization
        formattedFileName = formattedFileName.title()
    return formattedFileName

def test(filename, look_back, regressionModel):
    # load the dataset
    # For loop to read in files
    dataset = read_dataset(filename)
    # reshape into X=t and Y=t+3
    x, y = create_dataset(dataset, look_back)
    # For each test file
    testPredict = regressionModel.predict(x)
    # In order to report prediction error in original units, we mulitiply each
    #	prediction by 63 in order to "de-normalize"
    testPredictDenormed = testPredict * 63.0
    yDenormed = y * 63.0
    print(filename + ": " + str(yDenormed))
    # Use the root mean square error to report approximate number of lyme cases mispredicted
    #	per four-year reporting period
    mseTestScore = mean_squared_error(yDenormed, testPredictDenormed[:])
    rmseTestScore = math.sqrt(mseTestScore)
    print('Test Score: %.2f RMSE, %.2f MSE' % (rmseTestScore, mseTestScore))
    # Predict
    rge = range(look_back, len(dataset) - 1)
    plt.plot(dataset[:, 14] * 63.0, label='Ground Truth')
    plt.xlabel("Reporting Month")
    plt.ylabel("Reported Lyme Disease Cases")
    plt.title("Reported Lyme Disease in the " + deriveHealthDistrictName(filename)
              + " Health District 2012-2015")

    plt.plot(rge, (testPredict * 63.0), label='Predicted')
    plt.legend()
    return plt, rmseTestScore, mseTestScore

def determine_num_samples(training_directory):
    return len(
        [name for name in os.listdir(training_directory) if os.path.isfile(os.path.join(training_directory, name))])

def accrue_knowledge(look_back, training_directory, testing_directory, alpha, num_training_samples, num_epochs):
    # Train network
    trainingData = None
    trainingDataLabels = None
    for filename in os.listdir(training_directory):
        # load the dataset
        # For loop to read in files
        dataset = read_dataset(os.path.join(training_directory, filename))
        # reshape into X=t and Y=t+3
        x, y = create_dataset(dataset, look_back)
        if trainingData is None:
            trainingData = x
            trainingDataLabels = y
        else:
            trainingData = numpy.append(trainingData, x, 0)
            trainingDataLabels = numpy.append(trainingDataLabels, y, 0)
    #Build the regression model
    regressionModel = linear_model.ARDRegression()
    #regressionModel = neighbors.KNeighborsRegressor(n_neighbors=7)
    #regressionModel = svm.SVR(C=1.75,epsilon=0.05)
    regressionModel.fit(trainingData, trainingDataLabels)
    # Test network
    rmseScores = []
    for filename in os.listdir(testing_directory):
        # test data
        plt, rmseTestScore, mseTestScore = test(os.path.join(testing_directory, filename), look_back, regressionModel)
        rmseScores.append(rmseTestScore)
        # plot visualization and save to disk
        plt.savefig(filename + '_testing_performance.jpg')
        # clear plot for next iteration
        plt.clf()
    rmseAverage = 0
    rmseCounter = 0
    for rmseScore in rmseScores:
        rmseAverage += rmseScore
        rmseCounter += 1
    print('Average Score: %.5f RMSE' % (rmseAverage / rmseCounter))


def main():
    training_directory = '/home/ktyler/Documents/strider/lyme/python/data/lagged_train_pruned_kfold'
    testing_directory = '/home/ktyler/Documents/strider/lyme/python/data/lagged_test_pruned_kfold'
    look_back = 3  # Number of months to use for prediction, i.e.: Use months x-3, x-2, and x-1 of data to predict month x
    alpha = 1  # Used to calculate number of hidden nodes
    num_epochs = 220  # Number of training iterations
    num_training_samples = determine_num_samples(training_directory)
    accrue_knowledge(look_back, training_directory, testing_directory, alpha, num_training_samples, num_epochs)


numpy.random.seed(7)
main()